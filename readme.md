this is a small test repo to do ffmpeg streaming

### sockets
send pipes over tcp sockets

    $ ffserver -f ffserver.conf
    $ ffmpeg -r 60 -i tcp://0.0.0.0:30000?listen http://localhost:8090/feed1.ffm
    $ # use 0.0.0.0 as the ip to accept connections from outside the machine

start application, have it connect to `localhost:30000` and start writing bytes


### pipes
make pipe so application can write to it

    $ # you can link pipes across directories with ln -s
    $ mkfifo frame.png.pipe

start ffserver [see ffserver.conf for config]

    $ ffserver -f ffserver.conf

start ffmpeg listening [need to do this before application opens pipe]

    $ ffmpeg -r 60 -i frame.png.pipe http://localhost:8090/feed1.ffm

then start application


### compile the sample application
sample application right now reads from `data/00004600.png` to `data/00004700.png` [I am using frames from Sintel]. The app simply reads all of the frames into memory, and outputs them to the created socket infinitely.

    $ clang++ -O2 -std=c++11 -o pngfilestream pngfilestream.cpp 


### other notes

    $ # output result to file
    $ ffmpeg -i frame.png.pipe -r 60 output.mp4
