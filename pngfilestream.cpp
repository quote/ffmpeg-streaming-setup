#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>
#include <cstring>
#include <sys/socket.h>
#include <netdb.h>
#include <unistd.h>
using namespace std;

int main(int args, char **argv) {

    // read 100 png images, store into memory

    vector<pair<char*, size_t> > png_files;

    int start_file = 4600, end_file = 4701;
    for(int i=start_file; i<end_file; i++) {
        stringstream ss;
        ss << "data/0000" << i << ".png";
        // use ios::ate to start at the end of the file
        ifstream file(ss.str(), ios::in | ios::binary | ios::ate);
        streampos size;

        if(file.is_open()) {
            // cout << ss.str() << " opened" << endl;
            size = file.tellg();
            char *buf = new char[size];
            file.seekg(0, ios::beg);
            file.read(buf, size);
            file.close();
            png_files.push_back(make_pair(buf, size));
            file.close();
            cout << ss.str() << ", " << size << endl;
        } else {
            // cout << ss.str() << " not opened" << endl;
        }
    }

    cout << "read " << png_files.size() << " files" << endl;

    cout << "preparing socket" << endl;

    struct addrinfo host_info;
    struct addrinfo *host_info_list;

    memset(&host_info, 0, sizeof(host_info));
    host_info.ai_family = AF_UNSPEC; // use either ipv4 or ipv6
    host_info.ai_socktype = SOCK_STREAM; // use tcp

    cout << "getting addrinfo" << endl;
    int res = getaddrinfo("localhost", "30000", &host_info, &host_info_list);
    if(res) {
        cout << "error when generating addrinfo for localhost:30000" << endl;
        return -1;
    }

    cout << "opening socket" << endl;
    int socketfd = socket(host_info_list->ai_family, host_info_list->ai_socktype, host_info_list->ai_protocol);
    if(socketfd == -1) {
        cout << "error when creating socket" << endl;
        return -1;
    }

    cout << "connecting to socket" << endl;
    res = connect(socketfd, host_info_list->ai_addr, host_info_list->ai_addrlen);
    if(res == -1) {
        cout << "connection error" << endl;
        return -1;
    }

    cout << "writing to socket" << endl;
    while(true) {
        for(int i=0; i<png_files.size(); i++) {
            cout << "writing frame " << i << endl;
            auto p = png_files[i];
            char *buf = p.first;
            size_t size = p.second;

            size_t res = send(socketfd, buf, size, 0);
            if(res != size) {
                cout << "error when sending frame " << i << endl;
            }
        }
    }

    freeaddrinfo(host_info_list);
    close(socketfd);

    return 0;
}

