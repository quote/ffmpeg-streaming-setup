#ifndef _streamutil_h
#define _streamutil_h
#include <string>
#include <cstdlib>
#include <sys/socket.h>
#include <netdb.h>
#include <unistd.h>

// bmp utils
// assumes 32bit colors are being used
// the struct formats are basically copied from windows documentation
namespace bmp {

struct __attribute__((__packed__)) bmpfileheader {
    uint16_t type; // fixed to 'BM'
    uint32_t size; // fileheader size + bmpheader size + data size
    uint16_t res0; // ignore
    uint16_t res1; // ignore
    uint32_t offset; // fixed to sizeof(bmpfileheader) + sizeof(bmpinfoheader)

    bmpfileheader() :
        type(0x4d42), size(0), res0(0), res1(0), offset(0)
    {}
};

struct __attribute__((__packed__)) bmpinfoheader {
    uint32_t size; // size of infoheader
    int32_t width; // image width
    int32_t height; // image height
    uint16_t planes; // 1 plane
    uint16_t bitcount; // 32bit (rgba)
    uint32_t compression; // ignore - 0
    uint32_t sizeimage; // width * height * 4 bytes (rgba)
    int32_t hres; // ignore - just putting 1000
    int32_t vres; // ignore - just putting 1000
    uint32_t colors; // ignore - 0
    uint32_t colorsimportant; // ignore - 0

    bmpinfoheader() :
        size(sizeof(bmpinfoheader)), width(0), height(0), planes(1), bitcount(32),
        compression(0), sizeimage(0), hres(1000), vres(1000),
        colors(0), colorsimportant(0)
    {}
};

struct __attribute__((__packed__)) bmpheader {
    struct bmpfileheader fh;
    struct bmpinfoheader ih;

    // modify both header parts to reflect the size of the image
    void set_size(const int32_t width, const int32_t height) {
        const uint32_t fhsize = sizeof(bmpfileheader);
        const uint32_t ihsize = sizeof(bmpinfoheader);
        const uint32_t datasize = width * height * 4;

        fh.size = fhsize + ihsize + datasize;
        fh.offset = fhsize + ihsize;
        ih.width = width;
        ih.height = height;
        ih.sizeimage = datasize;
    }
};

// Does an in-memory replacement of r and b channels
// Uses const_cast and reinterpret_cast to open the pointer for writing
void rgba_to_bgra(const void* rgba, const int32_t width, const int32_t height) {
    void *bgra_v = const_cast<void*>(rgba);
    char *bgra = reinterpret_cast<char*>(bgra_v);

    const int32_t size = width * height * 4;
    for(int32_t i=0; i<size; i+=4) {
        const char t = bgra[i];
        bgra[i] = bgra[i+2];
        bgra[i+2] = t;
    }
}

} // end namespace bmp


// helper class to initialize a socket and write bmp images to it
struct stream_s {
    bool active; // if true, socket has been initialized successfully
    struct addrinfo host_info;
    struct addrinfo *host_info_list;
    int socketfd; // opened socket
    int frame; // current frame (for debugging purposes)
    bmp::bmpheader bmph; // bmp header - mostly static with defaults, but width/height are set each write call

    stream_s() :
        active(false), host_info_list(0), socketfd(0), frame(0)
    {}

    ~stream_s() {
        closestream();
    }

    // initializes the stream, resets the bmp header structs
    // if everything works, active is set to true
    // gets ffmpeg host information from STREAM_HOST and STREAM_PORT
    bool init() {
        if(active)
            return true;

        // set host information
        memset(&host_info, 0, sizeof(host_info));
        host_info.ai_family = AF_UNSPEC;
        host_info.ai_socktype = SOCK_STREAM;

        char *host = getenv("STREAM_HOST");
        char *port = getenv("STREAM_PORT");

        if(!host || !port) {
            std::cout << "error PVO_HOST or PVO_PORT missing" << std::endl;
            return false;
        }

        // std::cout << "getting ardinfo for " << std::string(host) << ":" << std::string(port) << std::endl;
        int res = getaddrinfo(host, port, &host_info, &host_info_list);
        if(res) {
            std::cout << "error when generating addrinfo for " << std::string(host) << ":" << std::string(port) << std::endl;
            return false;
        }

        // std::cout << "opening socket" << std::endl;
        socketfd = socket(host_info_list->ai_family, host_info_list->ai_socktype, host_info_list->ai_protocol);
        if(socketfd == -1) {
            std::cout << "error when creating socket" << std::endl;
            return false;
        }

        // std::cout << "connecting to socket " << socketfd << std::endl;
        res = connect(socketfd, host_info_list->ai_addr, host_info_list->ai_addrlen);
        if(res == -1) {
            // std::cout << "connection error with socket " << socketfd << std::endl;
            return false;
        }

        frame = 0;
        active = true;

        // reset the bmpheader file, will set sizes before rendering
        bmph = bmp::bmpheader();
        return true;
    }

    // write the rgba image buffer [width * height] to the active socket [if its opened]
    // shutdown socket and set active to false if a write fails
    bool write(const void* rgba, const int32_t width, const int32_t height) {
        if(!active)
            return false;

        std::cout << "writing frame " << frame << ", " << width << "x" << height << std::endl;

        // swap red and blue channels
        // bmp::rgba_to_bgra(rgba, width, height);

        bmph.set_size(width, height);
        size_t size = sizeof(bmph);
        size_t res = send(socketfd, &bmph, size, 0);
        if(res != size)  {
            std::cout << "error when sending frame " << frame << " header (" << size << " != " << res << ")" << std::endl;
            closestream();
            return false;
        }

        size = width * height * 4;
        res = send(socketfd, rgba, size, 0);
        if(res != size) {
            std::cout << "error when sending frame " << frame << " data (" << size << " != " << res << ")" << std::endl;
            closestream();
            return false;
        }

        frame++;
        return true;
    }

    // shutdown the stream, sets active to false
    void closestream() {
        std::cout << "closing socket " << socketfd << std::endl;
        close(socketfd);
        socketfd = -1;
        active = false;
    }

};

#endif // _streamutil_h
